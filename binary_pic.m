function bp = binary_pic( img, threshold)
% distinguishes object from background
    bp = img > threshold;
end


function classes = classify_all(vs,N,Means,Invcors,Dim,Aprioris)
% classifies a set of testing samples

   classes = [];
   for i = 1:length(vs)
       classes = [classes classify(vs(i,:),N,Means,Invcors,Dim,Aprioris)];
   end

end


% load the images we are given
load_pics;

% get the normalized RGB pics
for i = 1:length(pics)
    normalized_pics{i} = normalizeRGB(pics{i});
end

% extract background and normalize it as well
bgr = extract_background(pics);
normalized_bgr = normalizeRGB(bgr);

% now we can produce binary images from the normalized ones
% where each object pixel is 1 and each background pixel is 0
bps = generate_bp(normalized_pics,normalized_bgr,0);

% this takes the properties for all objects in each picture (Area,
% Perimeter, Centroid, PixelList)
props = gen_props(bps);

% now we have the objects extracted, so we are ready to classify

% we will give the true labels first to each object in each image
% the 0s are objects we won't be assigning classes so - 
% either noise or objects stacked over one another
classes{1} = [8, 6, 7, 9, 9, 10, 10, 7, 4, 7, 1, 5];
classes{2} = [8, 9, 6, 7, 4, 6, 10, 1, 6, 9, 8, 10, 2, 7, 5, 1, 3];
classes{3} = [8, 4, 6, 7, 1, 4, 10, 4, 8, 10, 2, 3, 1];
classes{4} = [2, 6, 2, 1, 4, 8, 5, 9, 7, 8, 9, 1, 6];
classes{5} = [9, 2, 6, 7, 10, 9, 8, 4, 4, 1, 6, 8];
classes{6} = [9, 8, 2, 6, 7, 1, 10, 4, 5, 3, 7, 4, 7, 5];
classes{7} = [6, 9, 8, 2, 10, 2, 5, 1, 4, 5, 7, 3, 7, 7, 4];
classes{8} = [6, 2, 8, 10, 7, 4, 3, 0, 1, 7, 7, 5, 6, 4];
classes{9} = [6, 2, 10, 8, 9, 7, 1, 7, 3, 8, 2, 6, 2];
classes{10} = [0, 8, 7, 9, 9, 10, 10, 7, 7, 0, 5];
classes{11} = [0, 7, 0, 0, 10, 7, 7, 0, 5];
classes{12} = [6, 3, 7, 2, 7, 9, 0, 7, 4, 1];
classes{13} = [6, 3, 7, 7, 0, 0, 1, 7, 4, 1, 10];
classes{14} = [6, 3, 7, 0, 9, 2, 5, 0, 4, 1, 7, 4];


% this goes through all objects to generate the training data - 
% a single vector for each object
cnt = 1;
vecs = zeros(164,6);
cs = zeros(164, 1);
for i = 1:14
    
    objects = props{i};
    for j = 1:length(objects)
        
        % take the current object and skip it it's assigned 0 for a class
        cur_obj = objects(j);
        if classes{i}(j)==0 
            continue;
        end
        
        % get the compactness and moments for this object
        vecs(cnt,1:3) = getproperties(cur_obj.Area, cur_obj.Perimeter, cur_obj.Centroid, cur_obj.PixelList);
        
        % get the pixels of this object and look at their colors back in
        % the normalized picture; we can get the mean value for each color channel
        % and use these as additional features for classification purposes
        pl = cur_obj.PixelList;
        original_normalized_pic = normalized_pics{i};
        valr = 0; valg = 0; valb = 0;
        for k = 1:length(pl)
            valr = valr + int64(original_normalized_pic(pl(k,2),pl(k,1),1));
            valg = valg + int64(original_normalized_pic(pl(k,2),pl(k,1),2));
            valb = valb + int64(original_normalized_pic(pl(k,2),pl(k,1),3));
        end        
        vecs(cnt,4) = valr/length(pl);
        vecs(cnt,5) = valg/length(pl);
        vecs(cnt,6) = valb/length(pl);
        
        cs(cnt) = classes{i}(j);
        cnt = cnt+1;
        
    end
    
end

% we use 10-fold cross validation
% for each fold, we train the model on a portion of the available data
% and test on the rest
indices = crossvalind('KFold', 164, 10);
cp = classperf(cs);
for i = 1:10
    test = (indices == i);
    train = ~test;
    Mdl = fitcecoc(vecs(train,:),cs(train,:),'Coding','denserandom');
    cv = predict(Mdl,vecs(test,:));
    classperf(cp, cv, test);
end
cp
% we have information about our classifier's performance in cp

% this is for use in the demonstration session when we will be provided
% with new test data, so we can now train on all that's currently available
[model_m,model_invcors,model_aprioris] = buildmodel(6, vecs, 164, 10, cs);

count_money(pics, model_m, model_invcors,model_aprioris);


% % train the model
% [model_m,model_invcors,model_aprioris] = buildmodel(6, vecs, 81, 10, cs);
% 
% % classify
% cv = [];
% for i = 7:7
%     objects = props{i};
%     for j = 1:length(objects)
%         v = zeros(1,6); 
%         cur_obj = objects(j);
%         v(1:3) = getproperties(cur_obj.Area, cur_obj.Perimeter, cur_obj.Centroid, cur_obj.PixelList);
%         
%         pl = cur_obj.PixelList;
%         original_normalized_pic = normalized_pics{i};
%         valr = 0;
%         valg = 0;
%         valb = 0;
%         for k = 1:length(pl)
%             valr = valr + int64(original_normalized_pic(pl(k,2),pl(k,1),1));
%             valg = valg + int64(original_normalized_pic(pl(k,2),pl(k,1),2));
%             valb = valb + int64(original_normalized_pic(pl(k,2),pl(k,1),3));
%         end        
%         v(4) = valr/length(pl);
%         v(5) = valg/length(pl);
%         v(6) = valb/length(pl);
%         
%         
%         cv = [cv classify(v,10,model_m,model_invcors,6,model_aprioris)];
%         
%         new_img = zeros(size(bps{1}));
%         pl = cur_obj.PixelList;
%         for k = 1:length(pl)
%             new_img(pl(k,2),pl(k,1)) = 1;
%         end
%         %figure(j);
%         %imshow(new_img);
%         
%     end
% end

%p = {}
%for i =1:14
    %p{i} = normalized_pics{i} - normalized_bgr;

%pic1 = normalized_pics{1}(:,:,1);
%hist = dohist(pic1,0);
%t = findthresh(hist, 6, 0);
%pb1 = ~binary_pic(pic1, t);

%pic1 = normalized_pics{1}(:,:,2);
%hist = dohist(pic1,0);
%t = findthresh(hist, 6, 0);
%pb2 = ~binary_pic(pic1, t);

%pic1 = normalized_pics{1}(:,:,3);
%hist = dohist(pic1,0);
%t = findthresh(hist, 6, 0);
%pb3 = ~binary_pic(pic1, t);

%pb = (pb1 + pb2 + pb3)/3;

    %[bp,r,g,b] = bin_pic(p{i});
    %img1 = bwmorph(bp,'majority');
    %img2 = bwmorph(img1,'spur');
    %img3 = bwmorph(img2,'clean');
    %figure(i);
    %imshow(img3);
    
%end

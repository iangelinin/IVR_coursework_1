function pb = produce_binary( img )
% distinguishes object from background
    gimg = myjpgload(img,0);
    hist = dohist(gimg,0);
    t = findthresh(hist, 6, 0);
    pb = ~binary_pic(gimg, t);
end
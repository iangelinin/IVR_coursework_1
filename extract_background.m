function background = extract_background( pics )
%Extracts a background from cell array of photos 
%The function works with both grayscale and rgb pictures
%The function first packs all photos in a matrix and then computes the
%median for each pixel on over the 3rd or 4th dimension depending on the
%picture.
    pic_size = cat(2,size(pics{1}),[size(pics,2)]);
    background = zeros(pic_size,class(pics{1}));
    for i = 1:size(pics,2)
        if size(pic_size,2) == 4
            background(:,:,:,i) = pics{i};
        else
            background(:,:,i) = pics{i};
        end
    end
    if size(pic_size,2) == 4
        background = median(background,4);
    else
        background = median(background,3);
    end
    
end


% gets a given complex central moment value

function muv = complexmoment(centroid, pixels, u, v)

     rbar = centroid(1);
     cbar = centroid(2);
     n = length(pixels);
     momlist = zeros(n,1);
     for i = 1 : n
       c1 = complex(pixels(i,1) - rbar,pixels(i,2) - cbar);
       c2 = complex(pixels(i,1) - rbar,cbar - pixels(i,2));
       momlist(i) = c1^u * c2^v;
     end
     muv = sum(momlist);
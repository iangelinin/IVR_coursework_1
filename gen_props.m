function props = gen_props( bin_pics )
% givven a binary picture gen_props generates and returns
%'Area','Perimeter','Centroid','PixelList' for each object which has area
%above the mean area
for i =1:size(bin_pics,2)
    %generates the properties for each object found on the picture
    props{i} = regionprops(bin_pics{i},'Area','Perimeter','Centroid','PixelList');
    
    %finding the mean area
    m = mean([props{i}.Area]);
    
    %removing objects with area less than the mean as we consider them
    %noise
    props{i}([props{i}.Area]<m) = [];    
end

end


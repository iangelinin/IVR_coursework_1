function bp_a = generate_bp( pics,bgr,show )
%generate_bp Generates binary pictures given rgb picturess and background to be
%subtracted out of the them, if show is 1 the prodused pics will be shown
for i =1:size(pics,2)
    %subtracting the background
    pics{i} = pics{i} - bgr;
    
    %generating the binary picture using bin_pic function
    bp = bin_pic(pics{i});
    
    %using bwmorph to make the picture clearer and remove noise
    img1 = bwmorph(bp,'majority');
    img2 = bwmorph(img1,'spur');
    img3 = bwmorph(img2,'clean');
    
    %showing the picture if the show parameter is 1
    if show == 1
        figure(i);
        imshow(img3);
    end
    
    %returning the cell array with binary pics
    bp_a{i} = img3;
end

end


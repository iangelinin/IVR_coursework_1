function [bin_pic,r,g,b] = bin_pic( picture )
%bin_pic returns a binary pic of a supplied picture of objects with subtracted background,
%areas corresponding to obects have pixel values 1, and areas corresponding to the
%background have pixel values 3
 
%finding the mean and standard deviation of each channel
 sd1 = std2(picture(:,:,1));
 m1 = mean2(picture(:,:,1));
 
 sd2 = std2(picture(:,:,2));
 m2 = mean2(picture(:,:,2));
 
 sd3 = std2(picture(:,:,3));
 m3 = mean2(picture(:,:,3));
 
 %creating the thresholds for each channel
 threshold1 = 4*sd1;
 threshold2 = 2*sd2;
 threshold3 = sd3;
 
 %creating the masks and generating the final image
 r = picture(:,:,1) < m1 - threshold1 | picture(:,:,1) > m1 + threshold1;
 g = picture(:,:,2) < m2 - threshold2 | picture(:,:,2) > m2 + threshold2;
 b = picture(:,:,3) < m3 - threshold3 | picture(:,:,3) > m3 + threshold3;
 bp = r | g | b;
 bin_pic = bp;

end


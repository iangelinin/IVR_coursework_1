function [] = show_sep_obj(bp)
%show_sep_obj Produces separate binary image for each object of the
%supplied binary picture
   

  % extracting objects from picture
  objects = gen_props({bp});
  objects = objects{1};
  
   %for each object generate a binary picture
   for j = 1:length(objects)
       cur_obj = objects(j);
       new_img = zeros(size(bp));
       pl = cur_obj.PixelList;
       for k = 1:length(pl)
           new_img(pl(k,2),pl(k,1)) = 1;
       end
       figure(j);
       imshow(new_img);
   end
   
   %show the binary picure with all objects
   figure(length(objects)+1);
   imshow(bp);

end


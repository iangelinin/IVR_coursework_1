% gets property vector for a binary shape in an image

function vec = getproperties(area, perim, centroid, pixels)

    % compactness
    compactness = perim*perim/(4*pi*area);

    % get scale-normalized complex central moments
    c11 = complexmoment(centroid,pixels,1,1) / (area^2);
    c20 = complexmoment(centroid,pixels,2,0) / (area^2);
    c30 = complexmoment(centroid,pixels,3,0) / (area^2.5);
    c21 = complexmoment(centroid,pixels,2,1) / (area^2.5);
    c12 = complexmoment(centroid,pixels,1,2) / (area^2.5);

  % get invariants, scaled to [-1,1] range
    ci1 = real(c11);
    ci2 = real(1000*c21*c12);
    tmp = c20*c12*c12;
    ci3 = 10000*real(tmp);
    ci4 = 10000*imag(tmp);
    tmp = c30*c12*c12*c12;
    ci5 = 1000000*real(tmp);
    ci6 = 1000000*imag(tmp);

    vec = [compactness,ci1,ci3];
    

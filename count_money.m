function [amount] = count_money( pics, model_m, model_invcors, model_aprioris )
% Given a new set of pictures, this function counts the money on each of
% them, based on a classifier trained on the original images

    amount = [];

    % these are the monetary values each class represents
    values = [1, 2, 0.5, 0.2, 0.05, 0.75, 0.25, 0.02, 0, 0];

    % get the normalized RGB pics
    for i = 1:length(pics)
        normalized_pics{i} = normalizeRGB(pics{i});
    end

    % extract background and normalize it as well
    bgr = extract_background(pics);
    normalized_bgr = normalizeRGB(bgr);

    % now we can produce binary images from the normalized ones
    % where each object pixel is 1 and each background pixel is 0
    bps = generate_bp(normalized_pics,normalized_bgr,0);

    % this takes the properties for all objects in each picture (Area,
    % Perimeter, Centroid, PixelList)
    props = gen_props(bps);
    
    % iterate through the pictures
    for i = 1:length(pics)
    
        objects = props{i};
        vecs = zeros(length(objects),6);
        cnt = 1;
        
        % iterate through all objects in the current picture
        for j = 1:length(objects)
            
            % take the current object and get the compactness and moments
            % for it
            cur_obj = objects(j);
            vecs(cnt,1:3) = getproperties(cur_obj.Area, cur_obj.Perimeter, cur_obj.Centroid, cur_obj.PixelList);
        
            % get the pixels of this object and look at their colors back in
            % the normalized picture; we can get the mean value for each color channel
            % and use these as additional features for classification purposes
            pl = cur_obj.PixelList;
            original_normalized_pic = normalized_pics{i};
            valr = 0; valg = 0; valb = 0;
            for k = 1:length(pl)
                valr = valr + int64(original_normalized_pic(pl(k,2),pl(k,1),1));
                valg = valg + int64(original_normalized_pic(pl(k,2),pl(k,1),2));
                valb = valb + int64(original_normalized_pic(pl(k,2),pl(k,1),3));
            end        
            vecs(cnt,4) = valr/length(pl);
            vecs(cnt,5) = valg/length(pl);
            vecs(cnt,6) = valb/length(pl);
        
            cnt = cnt+1;
        
        end
        
        % classify the objects in the current picture
        cv = classify_all(vecs,10,model_m,model_invcors,6,model_aprioris);
        
        % sum up the money in the picture
        cnt = 0.0;
        for j = 1:length(cv)
            cnt = cnt + values(cv(j));
        end
        amount = [amount cnt];
    end

end

